﻿using System;
using System.Management.Automation;
using System.IO;
using System.Reflection;
using System.Text;

namespace Posh_Toggl
{
	[Cmdlet(VerbsCommon.Set, "TogglAuth")]
	public class SetTogglAuth : Cmdlet
	{
        [Parameter(Position = 0, Mandatory = true)]
        [ValidateNotNullOrEmpty()]
        public string APIToken { get; set; }

		protected override void ProcessRecord()
        {
			string auth = APIToken + ":api_token";
			byte[] bytes = Encoding.ASCII.GetBytes(auth);
			string base64 = Convert.ToBase64String(bytes);
            File.WriteAllText(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + Path.DirectorySeparatorChar + "TogglAuth.txt", base64);
        }
    }

	[Cmdlet(VerbsCommon.Get, "TogglAuth")]
    [OutputType(typeof(string))]
	public class GetTogglAuth : Cmdlet
    {
		protected override void ProcessRecord()
		{
            string authPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + Path.DirectorySeparatorChar + "TogglAuth.txt";
		    string authMethod = File.ReadAllText(authPath);
            WriteObject(authMethod);
	    }
	}
}
