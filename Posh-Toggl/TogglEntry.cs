﻿using System.Management.Automation;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System;
using Newtonsoft.Json;

namespace Posh_Toggl
{
    [Cmdlet(VerbsCommon.Get, "TogglEntry")]
    [OutputType(typeof(TogglEntry))]
    public class GetTogglEntry : Cmdlet
    {

		[Parameter(Position = 0, Mandatory = false)]
        [ValidateNotNullOrEmpty]
		public string StartDate { get; set; }

		[Parameter(Position = 1, Mandatory = false)]
		[ValidateNotNullOrEmpty]
		public string EndDate { get; set; }

        HttpClient client = new HttpClient();
        string urlParameters;

		protected override void BeginProcessing()
		{
            string header = "";
			foreach (var result in new GetTogglAuth().Invoke())
			{
				 header = result.ToString();
			}

            client.BaseAddress = new Uri("https://www.toggl.com/api/v8/time_entries");
            client.DefaultRequestHeaders.Authorization = 
                new AuthenticationHeaderValue("Basic", header);
            urlParameters = "?start_date=" + WebUtility.UrlEncode(StartDate) + "&end_date=" + WebUtility.UrlEncode(EndDate);
		}

        protected override void ProcessRecord()
        {
			HttpResponseMessage response = client.GetAsync(urlParameters).Result;
			if (response.IsSuccessStatusCode)
			{
                var json = response.Content.ReadAsStringAsync().Result;
                TogglEntry[] entries = JsonConvert.DeserializeObject<TogglEntry[]>(json);
                WriteObject(entries);
			}
			else
			{
                WriteObject(response.Content.ReadAsStringAsync().Result);
			}
		}
    }

    [Cmdlet(VerbsCommon.New, "TogglEntry")]
    public class NewTogglEntry : Cmdlet
    {

    }

    [Cmdlet(VerbsCommon.Remove, "TogglEntry")]
    public class RemoveTogglEntry : Cmdlet
    {

    }

    [Cmdlet(VerbsLifecycle.Start, "TogglEntry")]
    public class StartTogglEntry : Cmdlet
    {

    }

    [Cmdlet(VerbsLifecycle.Stop, "TogglEntry")]
    public class StopTogglEntry : Cmdlet
    {

    }

    [Cmdlet(VerbsData.Update, "TogglEntry")]
    public class UpdateTogglEntry : Cmdlet
    {

    }

    public class TogglEntry
    {
        public Int64 ID { get; set; }
        public Int64 WID { get; set; }
        public Int64 PID { get; set; }
        public Boolean Billable { get; set; }
        public string Start { get; set; }
        public string Stop { get; set; }
        public Int64 Duration { get; set; }
        public string Description { get; set; }
        public string[] Tags { get; set; }
        public string At { get; set; } 
    }
}

